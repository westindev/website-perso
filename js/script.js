/* Sections */
let about = document.querySelector('#about');
let skills = document.querySelector('#skills');
let portfolio = document.querySelector('#portfolio');
let workflow = document.querySelector('#workflow');
let contact = document.querySelector('#contact');



/* LOADER CHARGEMENT DE LA PAGE */
window.addEventListener('load', () => {
    setTimeout(showContent, 800);
})

function showContent() {
    document.querySelector('.loader-container').classList.add('hidden');
}

filterSelection('all');
function filterSelection(element) {
    let btn, i;
    btn = document.getElementsByClassName("filterDiv");
    if (element == "all")
        element = "";
    for (i = 0; i < btn.length; i++) {
        filterRemoveClass(btn[i], "show");
        if (btn[i].className.indexOf(element) > -1) {
            filterAddClass(btn[i], "show");
        }
    }
}


// Show filtered elements
function filterAddClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) == -1) {
            element.className += " " + arr2[i];
        }
    }
}

// Hide elements that are not selected
function filterRemoveClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}

// Add active class to the current control button (highlight it)
let containerFilter = document.querySelector('.container-filter');
let btns = containerFilter.getElementsByClassName("btns");
for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
        let current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
}



const show = (element) => {
    if (element.classList.contains('hidden'))
        element.classList.remove('hidden');
    else
        element.classList.add('hidden');
}


let mobileMenu = document.getElementById('mobile-menu');
let menuList = document.getElementsByClassName('page-link');

/* Handler navbar responsive */
const handlerMenu = () => {
    let hamburger = document.getElementById('hamburger');
    hamburger.addEventListener('click', () => {
        let icones = document.getElementsByClassName('icones-menu');
        show(mobileMenu);
        for (let btn of icones) {
            if (btn.classList.contains('hidden'))
                btn.classList.remove('hidden');
            else
                btn.classList.add('hidden');
        }
    });
};


/* Handler menu links */
const handlePageLink = () => {
    for (let link of menuList) {
        link.addEventListener('click', () => {
            console.log(link.getAttribute('dest'));
            console.log(document.getElementById(link.getAttribute('dest')));
            document.getElementById(link.getAttribute('dest')).scrollIntoView();
            show(mobileMenu);
            let icones = document.getElementsByClassName('icones-menu');
            for (let btn of icones) {
                if (btn.classList.contains('hidden'))
                    btn.classList.remove('hidden');
                else
                    btn.classList.add('hidden');
            }
        });
    }
};



contact.scrollIntoView(true);



handlerMenu();
handlePageLink();





const currentMenu = (attr) => {
    let listLink = document.getElementsByClassName('page-link');
    for (let link of listLink) {
        if (attr === link.getAttribute('dest')){
            link.classList.replace('text-white', 'text-orange-500');
            link.classList.add('bg-white');
        }
        else
            link.classList.replace('text-orange-500', 'text-white');
    }
}


window.addEventListener('scroll', () => {
    let pos_window = window.scrollY;
    let pos = pos_window;
    //console.log("pos_window: " + pos_window)
    //console.log("pos: " + pos)

    let about_rect = about.getBoundingClientRect();
    let skills_rect = skills.getBoundingClientRect();
    let portfo_rect = portfolio.getBoundingClientRect();
    let workflow_rect = workflow.getBoundingClientRect();
    let contact_rect = contact.getBoundingClientRect();


    /* barre horizontale de scroll */
    let nav_b_bar = document.getElementById('nav-b-bar');
    nav_b_bar.classList.replace('bg-gray-500', 'bg-orange-500');
    let winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let scrolled = (winScroll / height) * 100;
    nav_b_bar.style.width = `${scrolled}%`;


    // Link Highlighting
    if (pos > (about_rect.top - 170) + window.scrollY) {
        currentMenu('about');
    }
    if (pos > (skills_rect.top - 170) + window.scrollY) {
        currentMenu('skills');
    }
    if (pos > (portfo_rect.top -170) + window.scrollY) {
        currentMenu('portfolio');
    }
    if (pos > (workflow_rect.top -170) + window.scrollY) {
        currentMenu('workflow');
    }
    if (pos > (contact_rect.top -170) + window.scrollY) {
        currentMenu('contact');
    }
})

/* Sans Jquery */
//let isMobile;
/*
(function () {


    if (window.matchMedia("(max-width: 700px)").matches) {
        isMobile = true;
    }
    if (isMobile) {
        document.querySelector('nav').classList.add('fixed');
    }
    else {
        document.querySelector('nav').classList.add('desk');
    }

    // NAV POSITION
    // offsetTop renvoie la distance entre l'élément courant et le haut du nœud offsetParent.
    let navPos = document.querySelector('nav').offsetTop;
    let lastPos = 0;
    let lockTimer;

    window.addEventListener('scroll', () => {
        let pos = window.scrollY;
        let pos2 = pos + 80;
        let el = document.body;
        let scrollBottom = pos + parseFloat(getComputedStyle(el, null).height.replace("px", ""));

        if (!isMobile) {
            let nav = document.querySelector('nav');
            if (pos >= navPos + parseFloat(getComputedStyle(nav, null).height.replace("px", "")) && lastPos < pos) {
                nav.classList.add('fixed');
            }
            if (pos < navPos && lastPos > pos) {
                nav.classList.remove('fixed');
            }
            lastPos = pos;
        }



        /* Sections */
        /*
        let about = document.querySelector('#about');
        let aboutRect = about.getBoundingClientRect();

        let work = document.querySelector('#work');
        let workRect = work.getBoundingClientRect();

        let workflow = document.querySelector('#workflow');
        let workflowRect = workflow.getBoundingClientRect();

        let contact = document.querySelector('#contact');
        let contactRect = contact.getBoundingClientRect();


        // Link Highlighting
        if (pos2 > aboutRect.top + window.scrollY) {
            highlightLink('about');
        }
        if (pos2 > workRect.top + window.scrollY) {
            highlightLink('work');
        }
        /*  if (pos2 > $('#profile').offset().top) {
            highlightLink('profile');
        } */
        /*
        if (pos2 > workflowRect.top + window.scrollY) {
            highlightLink('workflow');
        }
        if (pos2 > contactRect.top + window.scrollY) {
            highlightLink('contact');
        }

        // Prevent Hover on Scroll
        clearTimeout(lockTimer);

        if (!document.body.classList.contains('disable-hover')) {
            document.body.classList.add('disable-hover');
        }

        lockTimer = setTimeout(function () {
            document.body.classList.remove('disable-hover');
        }, 500);
    })

    function highlightLink(anchor) {
        /* Sélection des éléments avec classes actives */
        
        //let act = document.querySelector('.page-link .active');
        /* delete classe active */
        //act.classList.remove('active');

        /* Sélection des liens puis verif match avec anchor et attribut dest */
        //let anch = document.querySelectorAll('.page-link');
        //anch.forEach(element => {
         //   if (element.getAttribute('dest') == anchor) {
          //      element.classList.add('active');
            //}
       // })
   // }

//})

/*
$(function () {


    // Sticky Nav on Mobile
    /* if (isMobile) {
        $('nav').addClass('fixed');
    } else {
        $('nav').addClass('desk');
    } */

/* // NAV POSITION
var navPos = $('nav').position().top;
var lastPos = 0;
var lockTimer;

$(window).on('scroll', function () {
    var pos = $(window).scrollTop();
    var pos2 = pos + 80;
    var scrollBottom = pos + $(window).height();

    if (!isMobile) {
        if (pos >= navPos + $('nav').height() && lastPos < pos) {
            $('nav').addClass('fixed');
        }
        if (pos < navPos && lastPos > pos) {
            $('nav').removeClass('fixed');
        }
        lastPos = pos;
    }

    // Link Highlighting
    if (pos2 > $('#home').offset().top) {
        highlightLink('home');
    }
    if (pos2 > $('#about').offset().top) {
        highlightLink('about');
    }
    if (pos2 > $('#work').offset().top) {
        highlightLink('work');
    }
   /*  if (pos2 > $('#profile').offset().top) {
        highlightLink('profile');
    } */
/*if (pos2 > $('#workflow').offset().top) {
    highlightLink('workflow');
}
if (
    pos2 > $('#contact').offset().top ||
    pos + $(window).height() === $(document).height()
) {
    highlightLink('contact');
}

// Prevent Hover on Scroll
clearTimeout(lockTimer);
if (!$('body').hasClass('disable-hover')) {
    $('body').addClass('disable-hover');
}

lockTimer = setTimeout(function () {
    $('body').removeClass('disable-hover');
}, 500);
}); */

/* function highlightLink(anchor) {
    $('nav .active').removeClass('active');
    $('nav')
        .find('[dest="' + anchor + '"]')
        .addClass('active');
} */

    // EVENT HANDLERS
/*
    $('.page-link').click(function () {
        var anchor = $(this).attr('dest');
        $('.link-list').removeClass('visible');
        /* Retirer la classe 'opened' du menu une fois cliqué sur un lien */
/*
$('.mdi-menu').removeClass('opened');

$('nav span').removeClass('active');
$('nav')
    .find('[dest="' + anchor + '"]')
    .addClass('active');

$('html, body').animate(
    {
        scrollTop: $('#' + anchor).offset().top
    },
    400
);
});

$('.mdi-menu').click(function () {
$('.link-list').toggleClass('visible');
});

$('.blog-wrap').hover(
function () {
    $('.blog-wrap')
        .not(this)
        .addClass('fade');
    $(this).addClass('hover');
},
function () {
    $(this).removeClass('hover');
    $('.blog-wrap').removeClass('fade');
}
);


// SCROLL ANIMATIONS
function onScrollInit(items, elemTrigger) {
var offset = $(window).height() / 1.6;
items.each(function () {
    var elem = $(this),
        animationClass = elem.attr('data-animation'),
        animationDelay = elem.attr('data-delay');

    elem.css({
        '-webkit-animation-delay': animationDelay,
        '-moz-animation-delay': animationDelay,
        'animation-delay': animationDelay
    });

    var trigger = elemTrigger ? trigger : elem;

    trigger.waypoint(
        function () {
            elem.addClass('animated').addClass(animationClass);
            if (elem.get(0).id === 'gallery') mixClear(); //OPTIONAL
        },
        {
            triggerOnce: true,
            offset: offset
        }
    );
});
}

setTimeout(function () {
onScrollInit($('.waypoint'));
}, 10);


$('#close').click(function () {
$('#success').removeClass('expand');
});
});
*/
