// set the target element that will be collapsed or expanded (eg. navbar menu)
const targetEl = document.getElementById('mobile-menu');

// optionally set a trigger element (eg. a button, hamburger icon)
const triggerEl = document.getElementById('hamburger');

// optional options with default values and callback functions
const options = {
  triggerEl: triggerEl,
  onCollapse: () => {
      console.log('element has been collapsed')
  },
  onExpand: () => {
      console.log('element has been expanded')
  },
  onToggle: () => {
      console.log('element has been toggled')
  }
};

